package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	protoc "bitbucket.org/Amartha/example-user/delivery/pb"
	userSrv "bitbucket.org/Amartha/example-user/service"
)

type server struct {
	service userSrv.UserService
}

func NewUserServerGRPC(gServer *grpc.Server, userSrv userSrv.UserService) {
	// RegisterUserHandlerServer using Server struct
	userServer := &server{
		service: userSrv,
	}
	protoc.RegisterUserHandlerServer(gServer, userServer)

	reflection.Register(gServer)
}
