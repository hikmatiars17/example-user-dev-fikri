module bitbucket.org/Amartha/example-user

go 1.16

require (
	bitbucket.org/Amartha/example-article v0.0.0-20220223065920-8e3b8e8d11e2
	github.com/budhip/common v1.0.6
	github.com/budhip/env-config v0.0.0-20220220182514-d514392c5e9c
	github.com/bwmarrin/snowflake v0.3.0
	github.com/gofiber/fiber/v2 v2.28.0
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.7.3
	github.com/lib/pq v1.10.4
	go.uber.org/zap v1.20.0
	google.golang.org/grpc v1.44.0
	google.golang.org/protobuf v1.27.1
)
