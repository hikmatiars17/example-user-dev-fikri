-- +goose Up
-- +goose StatementBegin
ALTER TABLE "user"
    ADD COLUMN article_viewed integer(10) DEFAULT 0;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
alter table "user" drop column article_viewed;
-- +goose StatementEnd
