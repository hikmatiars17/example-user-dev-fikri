package repository

import (
	"database/sql"

	"bitbucket.org/Amartha/example-user/model"
)

type postgreRepository struct {
	Conn *sql.DB
}

func NewPostgreRepository(conn *sql.DB) PostgreUserRepository {
	return &postgreRepository{conn}
}

type PostgreUserRepository interface {
	StoreUser(a *model.User) error
	GetUserByID(id int64) (*model.User, error)
	UpdateArticleViewedByEmail(email, updatedAt string) error
}

type RedisRepository interface {
}
